// organise - infinit s roll 
if (document.querySelector('#infinite-list')) {
    var base_url = window.location.href;
    var elm = document.querySelector('#infinite-list');
    // loader = document.querySelector('#loaderPost');
    var page = elm.dataset.page;
    var height = window.innerHeight || document.documentElement.clientHeight || document.body.clientHeight;
    load = false;
    ticking = false;
    window.addEventListener('scroll', function(e) {
        if (!ticking) {
            window.setTimeout(function() {
                ticking = false;
                //Do something
                if (!load && (elm.offsetTop + elm.clientHeight) < (window.scrollY + height)) { //scroll > bas de infinite-list
                    //inserer les éléments suivants
                    if (page >= elm.dataset.nbPageMax) {
                        console.log(load);
                    } else {
                        load = true;
                        readMorePost();
                    }
                }
            }, 300); //fréquence du scroll
        }
        ticking = true;
    });
}


function readMorePost() {
    page++;
    // loader.classList.add("active");
    var formData = new FormData();
    formData.append("page", page);
    formData.append("cpt", elm.dataset.cpt);
    formData.append("taxo", elm.dataset.taxo);
    formData.append("taxo_tag_var", elm.dataset.taxo_tag);

    console.log("readMorePost");

    xhr = new XMLHttpRequest();
    xhr.open('POST', resturl + 'scroll', true);
    xhr.onload = function() {
        if (xhr.status === 200) {
            load = false;
            console.log(xhr.response);
            elm.insertAdjacentHTML('beforeend', xhr.response);
            // window.history.replaceState("", "", elm.dataset.url + "page/" + page + "/");
            // loader.classList.remove("active");
        }
    };
    xhr.send(formData);
}


// filter on tpl-post
if(document.getElementById('form-filter')){
    var filter_category = document.getElementById('filter-category');
    filter_category.addEventListener('change', function(){
        document.getElementById('form-filter').submit();
    });
}


// Ajax to load more post 
if (document.getElementById("archive-listing")) {  

    var page = 1;   
    
    if(document.getElementById("more_post")) {
        var btn_load_more = document.getElementById("more_post");

        btn_load_more.addEventListener("click", function() {
            page++;   
            // console.log(page);
            // console.log(max_num_pages);
            if (page >= max_num_pages) { 
                btn_load_more.style.display = 'none';
            }
            var formData = new FormData();
            //ajout variable ds formulaire
            formData.append("page", page);
            formData.append("category-slug", category);
            xhr = new XMLHttpRequest();
            xhr.open('POST', resturl+'read_more_post');
            xhr.onload = function() {
                if (xhr.status === 200) {
                    elm = document.getElementById('display-posts');
                    elm.insertAdjacentHTML('beforeend', xhr.response);
                    // console.log(xhr.response);
                }
            };
            xhr.send(formData);
        });  
    }
    
}

// Ajax to load more resource 
if (document.getElementById("archive-listing-resources")) {   

    var page = 1;     
    
    if(document.getElementById("more_resource")) {
        var btn_load_more = document.getElementById("more_resource");

        btn_load_more.addEventListener("click", function() {
            page++;   
            // console.log(page);
            // console.log(max_num_pages);
            if (page >= max_num_pages) { 
                btn_load_more.style.display = 'none';
            }
            var formData = new FormData();
            //ajout variable ds formulaire
            formData.append("page", page);
            formData.append("category-slug", category);
            xhr = new XMLHttpRequest();
            xhr.open('POST', resturl+'read_more_resource');
            xhr.onload = function() {
                if (xhr.status === 200) {
                    elm = document.getElementById('display-resources');
                    elm.insertAdjacentHTML('beforeend', xhr.response);
                    addModaleListener();
                    // console.log(xhr.response);
                }
            };
            xhr.send(formData);
        });   
    }
    
}


