<?php
/**
 * Template part for displaying page single-member in page.php
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/
 *
 */

?>

<?php 
	$author = get_user_by( 'slug', get_query_var( 'author_name' ) );
	// var_dump($author->roles);

	if ( !in_array( 'member', (array) $author->roles ) ) {
		wp_redirect( home_url(), 301 );
		die();
	} 
?>

<?php get_header(); ?>

<?php 
	$user_image = get_field('image'); 
	$image_by_default = get_field('logo', 'options');
	$user_sector = get_field('sector');
	$user_desc = get_the_author_meta('description');
	$user_site = get_field('site');
	$user_twitter = get_field('twitter');
	$user_linkdin = get_field('linkdin');
	$user_facebook = get_field('facebook');
?>

<article>
	
	<div class="soloMember">

		<!-- Fil d'Ariane -->
		<?php if (function_exists('the_breadcrumb')) the_breadcrumb(); ?>

		<h2><?php the_author();?></h2>

		<div class="containerMember">

			<!-- img -->
			<div class="imgMember">
				<?php if (!empty ($user_image)) : 
					echo wp_get_attachment_image( $user_image, 'icon-membersolo' ); 
				else :
					echo wp_get_attachment_image( $image_by_default, 'icon-membersolo' );
				endif; ?>  
			</div>
			<!-- sector & desc -->
			<div class="memberText">
				<?php if (!empty($user_sector)) :?>
					<p><?php echo $user_sector['label']; ?></p>
				<?php endif; ?>

				<?php if (!empty($user_desc)):?>
					<p><?php echo $user_desc; ?></p>
				<?php endif; ?>

			</div> 
		
		</div>

		<!-- contact -->
		<div class="contactMember">
			<?php if (!empty($user_site)) :
				$link_url_site = $user_site['url'];
				$link_title_site = $user_site['title'];
				$link_target_site = $user_site['target'] ? $user_site['target'] : '_self';
				?>
				
				<a class="" href="<?php echo esc_url( $link_url_site ); ?>" target="<?php echo esc_attr( $link_target_site ); ?>">
					<img aria-hidden="true" src="<?php echo get_template_directory_uri(); ?>/image/site.svg" height="24" width="24">
				</a>
			<?php endif; ?>


			<?php if (!empty($user_twitter)) :
				$link_url_twitter = $user_twitter['url'];
				$link_title_twitter = $user_twitter['title'];
				$link_target_twitter = $user_twitter['target'] ? $user_twitter['target'] : '_self';
				?>
				
				<a class="" href="<?php echo esc_url( $link_url_twitter ); ?>" target="<?php echo esc_attr( $link_target_twitter ); ?>">
					<img aria-hidden="true" src="<?php echo get_template_directory_uri(); ?>/image/twitter.svg" height="24" width="24">
				</a>
			<?php endif; ?>

			<?php if (!empty($user_linkdin)) :
				$link_url_linkdin = $user_linkdin['url'];
				$link_title_linkdin = $user_linkdin['title'];
				$link_target_linkdin = $user_linkdin['target'] ? $user_linkdin['target'] : '_self';
				?>
				
				<a class="" href="<?php echo esc_url( $link_url_linkdin ); ?>" target="<?php echo esc_attr( $link_target_linkdin ); ?>">
					<img aria-hidden="true" src="<?php echo get_template_directory_uri(); ?>/image/linkedin.svg" height="24" width="24">
				</a>
			<?php endif; ?>

			<?php if (!empty($user_facebook)) :
				$link_url_facebook = $user_facebook['url'];
				$link_title_facebook = $user_facebook['title'];
				$link_target_facebook = $user_facebook['target'] ? $user_facebook['target'] : '_self';
				?>
				
				<a class="" href="<?php echo esc_url( $link_url_facebook ); ?>" target="<?php echo esc_attr( $link_target_facebook ); ?>">
					<img aria-hidden="true" src="<?php echo get_template_directory_uri(); ?>/image/facebook.svg" height="24" width="24">
				</a>
			<?php endif; ?>
		</div>

	</div>

</article>

<?php get_footer(); ?>



