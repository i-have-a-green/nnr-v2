<?php get_header(); ?>

    <!-- <?php if ( !is_front_page() ){
		echo '<header class="page-title">';?>
		
		<?php 
		the_title('<h1 class="center">', '</h1>');
		echo '</header>';
	} ?> -->

<!-- Begining of the loop -->
<?php if (have_posts()) : while (have_posts()) : the_post(); ?>


<!-- End of the loop -->
<?php endwhile; endif;?>
<main id="raw-content">
	<!-- Fil d'Ariane -->
	<?php if (function_exists('the_breadcrumb')) the_breadcrumb(); ?>
	<?php the_content(); ?>
</main>

<?php
get_footer();
