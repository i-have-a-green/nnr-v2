<?php
/**
 * The template for displaying archive pages
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/
 *
 */

get_header();
?>

<!-- Header -->
<header class="page-title">
	<?php //wpBreadcrumb(); ?>
	<h1 class="center">
		#<?php echo single_cat_title( '', false );?>
	</h1>
</header>

<div class="center narrow-wrapper archive-info">
	<?php echo category_description(); ?>
</div>

<!-- pour le scroll -->
<?php $num_page = (get_query_var("paged") ? get_query_var("paged") : 1);?>
<?php //var_dump($wp_query->found_posts); ?>
				
<!-- Listing Universal -->
<section>

	<!-- wrapper -->
	<div class="listing-universal narrow-wrapper v-padding-small"

		data-cpt=""
		data-page="<?php echo $num_page;?>"
		data-nb-page-max="<?php echo ceil(($wp_query->found_posts)/(get_option('posts_per_page' ))); ?>"
		data-url="<?php echo get_category_link(get_queried_object()->term_id);?>"
		data-taxo=""
		data-taxo_tag="<?php echo get_queried_object()->term_id;?>"

		id="infinite-list">

		<?php if ( have_posts() ) : ?>

			<p class="h1-like center">
				<?php esc_html_e('Tous les articles sur le sujet : ', 'ihag')?>
			</p>

			<?php
			/* Start the Loop */
			while ( have_posts() ) :
			?>

			<?php 
				the_post();
				get_template_part( 'template-parts/archive', get_post_type() );
			endwhile;
			?>

		<?php else : ?>

			<p class="h1-like center">
				<?php esc_html_e('Aucun article correspondant', 'ihag')?>
			</p>

			<?php get_template_part( 'template-parts/content', 'none' ); ?>
	
		<?php endif; ?>

	</div><!-- /wrapper -->

</section><!-- End of Listing Archive -->

<?php
get_footer();
