	</div><!-- div #content -->


	<div class="footer__before">

		<!-- to newsletter Mailjet -->

			<h2>Gardons contact !</h2>
			<div class="footer__newsletter">
				<h3>S'inscrire à notre newsletter</h3>
				<div class="email">
					<form action="<?php the_permalink(); ?>" method="post" name="newsletterForm" id="newsletterForm" >
						<label for="emailNewsletter"><?php esc_html_e('Adresse mail', 'ihag')?></label>
						<div class="footer__email-input" id="newsInput">
							<!-- honeypot -->
							<input type="hidden" name="honeypot" value="">
							<input type="email" name="emailNewsletter" id="emailNewsletter" placeholder="<?php esc_html_e('nom.prenom@exemple.com', 'ihag')?>" required value="">
							<input type="submit" class="button form-item" id="sendEmail" value="<?php _e('S\'inscrire', 'cwcud'); ?>">
						</div>
					</form>
				</div>
				<div id="ResponseMessageNewsletter" class="ResponseMessageNewsletter">
					Merci, votre inscription a été enregistrée.
				</div>
			</div>

		<!-- end mailjet -->

		<div class="footer__slack">
			<h3>Notre Slack</h3>
			<div class="slack">
				<p>Faîtes partie de l’aventure NNR et
				bénéficiez de notre veille quotidienne</p>
				<a href="#" class="button">Rejoindre le Slack</a>
			</div>
		</div>
	</div>
	
	<footer id="footer">
		
		<div class="">

			<!-- Footer Part 1 : Logo + RS -->
			<nav class="footer-logo">
				<!-- #footer-link -->
				<!--
				<a id="footer-link" href="<?php echo home_url() ?>" title="<?php esc_html_e('Lien vers la page d\'accueil', 'ihag')?>">
					<?php 
					// Custom Logo
					$logo = get_field('logo', 'options');
					$size = 'icon-header';
					if( $logo ) { 
						?>
						<img alt="<?php esc_html_e('Logo', 'ihag')?>" src="<?php echo wp_get_attachment_image_url( $logo, $size )?>">
						<?php 
					}?>
				</a>
				-->
				<!-- réseaux sociaux -->
				<div class="footer-social">

				
					<?php 
					$link = get_field('slack', 'options');
					if( $link ): 
						$link_url = $link['url'];
						$link_title = $link['title'];
						$link_target = $link['target'] ? $link['target'] : '_self';
						?>
						<a href="<?php echo esc_url( $link_url ); ?>" target="<?php echo esc_attr( $link_target ); ?>" title="<?php echo esc_html( $link_title ); ?>">
							<!-- <img aria-hidden="true" src="<?php //echo get_template_directory_uri(); ?>/image/???.svg" height="24" width="24"> -->
							<?php _e('Slack','nnr'); ?>
						</a>
					<?php endif; ?>
					
					<?php 
					$link = get_field('meetup', 'options');
					if( $link ): 
						$link_url = $link['url'];
						$link_title = $link['title'];
						$link_target = $link['target'] ? $link['target'] : '_self';
						?>
						<a href="<?php echo esc_url( $link_url ); ?>" target="<?php echo esc_attr( $link_target ); ?>" title="<?php echo esc_html( $link_title ); ?>">
							<!-- <img aria-hidden="true" src="<?php //echo get_template_directory_uri(); ?>/image/???.svg" height="24" width="24"> -->
							<?php _e('Meetup','nnr'); ?>
						</a>
					<?php endif; ?>

					<?php 
					$link = get_field('youtube', 'options');
					if( $link ): 
						$link_url = $link['url'];
						$link_title = $link['title'];
						$link_target = $link['target'] ? $link['target'] : '_self';
						?>
						<a href="<?php echo esc_url( $link_url ); ?>" target="<?php echo esc_attr( $link_target ); ?>" title="<?php echo esc_html( $link_title ); ?>">
							<!-- <img aria-hidden="true" src="<?php //echo get_template_directory_uri(); ?>/image/youtube.svg" height="24" width="24"> -->
							<?php _e('Youtube','nnr'); ?>
						</a>
					<?php endif; ?>
					
					<?php 
					$link = get_field('linkedin', 'options');
					if( $link ): 
						$link_url = $link['url'];
						$link_title = $link['title'];
						$link_target = $link['target'] ? $link['target'] : '_self';
						?>
						<a href="<?php echo esc_url( $link_url ); ?>" target="<?php echo esc_attr( $link_target ); ?>" title="<?php echo esc_html( $link_title ); ?>">
							<!-- <img aria-hidden="true" src="<?php //echo get_template_directory_uri(); ?>/image/linkedin.svg" height="24" width="24"> -->
							<?php _e('LinkedIn','nnr'); ?>
						</a>
					<?php endif; ?>


					<?php 
					$link = get_field('twitter', 'options');
					if( $link ): 
						$link_url = $link['url'];
						$link_title = $link['title'];
						$link_target = $link['target'] ? $link['target'] : '_self';
						?>
						<a href="<?php echo esc_url( $link_url ); ?>" target="<?php echo esc_attr( $link_target ); ?>" title="<?php echo esc_html( $link_title ); ?>">
							<!-- <img aria-hidden="true" src="<?php //echo get_template_directory_uri(); ?>/image/twitter.svg" height="24" width="24"> -->
							<?php _e('Twitter','nnr'); ?>
						</a>
					<?php endif; ?>

				</div>
			</nav>

			<?php echo ihag_menu('footer'); ?>

			<!--  Footer Part 3 : Divers liens en footer -->
				
			<?php 
			$text_footer = get_field('content', 'options');
			if ($text_footer) { 
				?>
				<div id="footer-content" class="center">		
					<?php echo $text_footer; ?>
				</div>
				<?php
			}?>

		</div><!-- End of .wrapper -->
	</footer><!-- End of #footer -->

<?php wp_footer(); ?>

</body>
</html>
