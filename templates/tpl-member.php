<?php
/*
Template Name: tpl member
*/
?>

<?php get_header(); ?>
<?php if (have_posts()) : while (have_posts()) : the_post(); ?>

<main> 

	<header class="title__container">

        <!-- Fil d'Ariane -->
        <?php if (function_exists('the_breadcrumb')) the_breadcrumb(); ?>
		
		<!-- Titre-->
        <div class="title__content">
            <?php the_title('<h1>', '</h1>'); ?>

            <svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 621.12 72.73"><defs><style>.cls-1{fill:#f0a6ff;}</style></defs><g data-name="Layer 2"><g data-name="Layer 1"><polygon class="cls-1" points="466.72 72.73 358.72 18.73 313.72 72.73 205.72 18.73 160.72 72.73 52.72 18.73 11.52 68.17 0 58.56 48.8 0 156.8 54 201.8 0 309.8 54 354.8 0 462.8 54 507.8 0 621.12 56.66 614.41 70.07 511.72 18.73 466.72 72.73"/></g></g></svg>
        </div>

	</header>

    <!-- args filter -->
    <?php 
        $args_filter= array (
            'role' => 'member'
        ); 
    ?>

    <!-- filter -->
    <?php 
  
        $users_filter =  get_users($args_filter);

        $list = array();
        $list_slug = array();
        foreach ($users_filter as $user_filter) :
            $sector = get_field('sector', $user_filter);
            if(!in_array($sector['value'], $list_slug)){
                $list_slug[] = $sector['value'];
                $list[] = $sector;
            }
        endforeach;?>

        <form id="form-filter-user" action="<?php the_permalink(); ?>" method="GET">

            <label for="filter-user"></label>

            <select name="user-slug" id="filter-user">

                <option value="" <?php if (isset($_GET['user-slug'])){ selected($_GET['user-slug'], ""); } ?> ><?php _e('Secteurs', 'nnr'); ?></option>

                <?php foreach($list as $filter):?>

                    <option class="" value="<?php echo $filter['value']; ?>" <?php if (isset($_GET['user-slug'])) { selected($_GET['user-slug'], $filter['value']);} ?> ><?php _e($filter['label'] , 'nnr') ;?></option>
                    
                <?php endforeach; ?>
                    
            </select>

        </form>


	<section id="archive-listing-user" class="">
        <?php  $num_page = (get_query_var("paged") ? get_query_var("paged") : 1); ?>
		
		<div class="grid-post wrapper-medium" id="userContainer">
        
            <!-- args -->
            <?php $args= array (
                'role' => 'member',
                'number'    => -1
            ); 
            
            $user_query = new WP_User_Query($args);
            $users_to_display = array();
            $count = 0;
                
            if ( !empty ($user_query->get_results())) : 

                foreach ($user_query->get_results() as $user) : 

                    $user_sector = get_field('sector', $user);
                    
                    $is_display= false;

                    //  if no filter
                    if (!isset($_GET['user-slug']) || (isset($_GET['user-slug']) && empty($_GET['user-slug']))):
                        
                        $is_display = true;

                    //  if filter 
                    elseif ( $user_sector['value'] == $_GET['user-slug']) :
                        $is_display = true;
                    else :
                        $is_display = false;
                    endif;

                    if ($is_display) :

                        // change here to display X users by default
                        if($count < 6) : 

                            // user card
                            set_query_var('user', $user); 
                            get_template_part( 'template-parts/archive', 'member' );

                        else :

                            // to load more
                            if (!empty(wp_get_attachment_image_src(get_field('image', $user),'icon-member'))):
                                $image_src = wp_get_attachment_image_src(get_field('image', $user),'icon-member');
                            elseif (empty(wp_get_attachment_image_src(get_field('image', $user),'icon-member'))): 
                                $image_src = wp_get_attachment_image_src(get_field('imageFallback', 'options'), 'icon-member');
                            endif;
                            $users_to_display[] = array(
                                'image'     =>  $image_src[0],
                                'name'      => $user->display_name, 
                                'sector'    => $user_sector['label'],
                                'permalink' => get_author_posts_url($user->ID)
                            );
                        
                        endif; 

                        $count++;

                    endif;  

                endforeach;?>
 
              
                 
            <?php endif;?>

		</div>
		<?php if ($count > 1) :?>
            <!-- To display more users  -->
            <button class="button" id="more_users"><?php _e('Voir plus', 'nnr'); ?></button>
        <?php endif; ?>
        <script>
            var users_more = <?php echo json_encode ($users_to_display); ?>
        </script>
	</section>

	<?php the_content('<section id="raw-content">', '</section>');?>


</main>

<!-- End of the loop -->
<?php endwhile; endif;?>

<?php get_footer(); ?>