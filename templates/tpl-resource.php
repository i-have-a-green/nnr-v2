<?php
/*
Template Name: tpl ressources
*/
?>

<?php get_header(); ?>
<?php if (have_posts()) : while (have_posts()) : the_post(); ?>

<main> 

	<header class="title__container">
        <!-- Fil d'Ariane -->
        <?php if (function_exists('the_breadcrumb')) the_breadcrumb(); ?>
		
		<!-- Titre-->
        <div class="title__content">
            <?php the_title('<h1>', '</h1>'); ?>

            <svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 621.12 72.73"><defs><style>.cls-1{fill:#f0a6ff;}</style></defs><g data-name="Layer 2"><g data-name="Layer 1"><polygon class="cls-1" points="466.72 72.73 358.72 18.73 313.72 72.73 205.72 18.73 160.72 72.73 52.72 18.73 11.52 68.17 0 58.56 48.8 0 156.8 54 201.8 0 309.8 54 354.8 0 462.8 54 507.8 0 621.12 56.66 614.41 70.07 511.72 18.73 466.72 72.73"/></g></g></svg>
        </div>

	</header>

   
     <!-- filtre -->
    <?php 
    $my_taxonomies = get_terms(array(
        'taxonomy' => 'category_resource',
    ));
    ?>

    <form id="form-filter" action="<?php the_permalink(); ?>" method="GET">

        <label for="filter-category"></label>

        <select name="category-slug" id="filter-category">

            <option value="" <?php if (isset($_GET['category-slug'])){ selected($_GET['category-slug'], ""); } ?> ><?php _e('Categories', 'nnr'); ?></option>

            <?php foreach($my_taxonomies as $my_taxonomy):?>

                <option class="filter-taxonomy" value="<?php echo $my_taxonomy->slug; ?>" <?php if (isset($_GET['category-slug'])) { selected($_GET['category-slug'], $my_taxonomy->slug);} ?> ><?php _e($my_taxonomy->name, 'nnr') ;?></option>
                
            <?php endforeach;?>
                
        </select>

    </form>


	<section id="archive-listing-resources" class="wrapper-grid-list">
        <?php  $num_page = (get_query_var("paged") ? get_query_var("paged") : 1); ?>
		
		<div class="grid-post wrapper-medium">
			<?php            
            //by taxonomy term
            if(isset($_GET['category-slug']) && !empty($_GET['category-slug'])){
                $args = array(
                    'post_type' => 'resource',
                    'tax_query' => array(
                        array(
                            'taxonomy' => 'category_resource',
                            'field'    => 'slug',
                            'terms'    => array(sanitize_text_field($_GET['category-slug'])),
                        ),
                    )
                );
            }

            //by default
            else {
                $args = array(
                    'post_type' => 'resource',
                );
            }?>

            <div id="display-resources" class="display-grid"> 
                <?php query_posts($args);
                global $wp_query; 
                if ( have_posts() ) : while (have_posts()) : the_post();
                    get_template_part('template-parts/archive', "resource");
                endwhile; endif;?>
            </div>

            <!-- display more resource if enough resources -->
            <?php if($wp_query->max_num_pages > 1 ): ;?> 

                <button class="button" id="more_resource"><?php _e('Voir plus', 'nnr'); ?></button>
                 
                <script>
                    
                    <?php 
                    echo 'var max_num_pages= "'.$wp_query->max_num_pages.'";';

                    if(isset($_GET['category-slug']) && !empty($_GET['category-slug'])){
                        echo 'var category = "'.$_GET['category-slug'].'";';
                    }else{
                        echo 'var category = "";';
                    }?>
                </script>

            <?php endif;

            // stop propagation
            wp_reset_query();
			?>

		</div>
		
	</section>

	<?php the_content('<section id="raw-content">', '</section>');?>


</main>

<!-- End of the loop -->
<?php endwhile; endif;?>

<?php get_footer(); ?>