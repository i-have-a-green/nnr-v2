<?php
/*
Template Name: Contact
*/
?>

<?php get_header(); ?>


<header class="contact-header title__container">

  <!-- Fil d'Ariane -->
  <?php if (function_exists('the_breadcrumb')) the_breadcrumb(); ?>
  <!-- <?php //wpBreadcrumb(); ?> -->

  <!-- Titre-->
  <div class="title__content">
      <?php the_title('<h1>', '</h1>'); ?>

      <svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 621.12 72.73"><defs><style>.cls-1{fill:#f0a6ff;}</style></defs><g data-name="Layer 2"><g data-name="Layer 1"><polygon class="cls-1" points="466.72 72.73 358.72 18.73 313.72 72.73 205.72 18.73 160.72 72.73 52.72 18.73 11.52 68.17 0 58.56 48.8 0 156.8 54 201.8 0 309.8 54 354.8 0 462.8 54 507.8 0 621.12 56.66 614.41 70.07 511.72 18.73 466.72 72.73"/></g></g></svg>
  </div>

	</header>

  <!-- Same link as in footer -->
  <nav class="footer-logo">

    <div class="footer-social contact__social">

      <?php 
      $link = get_field('slack', 'options');
      if( $link ): 
        $link_url = $link['url'];
        $link_title = $link['title'];
        $link_target = $link['target'] ? $link['target'] : '_self';
        ?>
        <a href="<?php echo esc_url( $link_url ); ?>" target="<?php echo esc_attr( $link_target ); ?>" title="<?php echo esc_html( $link_title ); ?>">
          <!-- <img aria-hidden="true" src="<?php //echo get_template_directory_uri(); ?>/image/???.svg" height="24" width="24"> -->
          <?php _e('Slack','nnr'); ?>
        </a>
      <?php endif; ?>
      
      <?php 
      $link = get_field('meetup', 'options');
      if( $link ): 
        $link_url = $link['url'];
        $link_title = $link['title'];
        $link_target = $link['target'] ? $link['target'] : '_self';
        ?>
        <a href="<?php echo esc_url( $link_url ); ?>" target="<?php echo esc_attr( $link_target ); ?>" title="<?php echo esc_html( $link_title ); ?>">
          <!-- <img aria-hidden="true" src="<?php //echo get_template_directory_uri(); ?>/image/???.svg" height="24" width="24"> -->
          <?php _e('Meetup','nnr'); ?>
        </a>
      <?php endif; ?>

      <?php 
      $link = get_field('youtube', 'options');
      if( $link ): 
        $link_url = $link['url'];
        $link_title = $link['title'];
        $link_target = $link['target'] ? $link['target'] : '_self';
        ?>
        <a href="<?php echo esc_url( $link_url ); ?>" target="<?php echo esc_attr( $link_target ); ?>" title="<?php echo esc_html( $link_title ); ?>">
          <!-- <img aria-hidden="true" src="<?php //echo get_template_directory_uri(); ?>/image/youtube.svg" height="24" width="24"> -->
          <?php _e('Youtube','nnr'); ?>
        </a>
      <?php endif; ?>
      
      <?php 
      $link = get_field('linkedin', 'options');
      if( $link ): 
        $link_url = $link['url'];
        $link_title = $link['title'];
        $link_target = $link['target'] ? $link['target'] : '_self';
        ?>
        <a href="<?php echo esc_url( $link_url ); ?>" target="<?php echo esc_attr( $link_target ); ?>" title="<?php echo esc_html( $link_title ); ?>">
          <!-- <img aria-hidden="true" src="<?php //echo get_template_directory_uri(); ?>/image/linkedin.svg" height="24" width="24"> -->
          <?php _e('LinkedIn','nnr'); ?>
        </a>
      <?php endif; ?>


      <?php 
      $link = get_field('twitter', 'options');
      if( $link ): 
        $link_url = $link['url'];
        $link_title = $link['title'];
        $link_target = $link['target'] ? $link['target'] : '_self';
        ?>
        <a href="<?php echo esc_url( $link_url ); ?>" target="<?php echo esc_attr( $link_target ); ?>" title="<?php echo esc_html( $link_title ); ?>">
          <!-- <img aria-hidden="true" src="<?php //echo get_template_directory_uri(); ?>/image/twitter.svg" height="24" width="24"> -->
          <?php _e('Twitter','nnr'); ?>
        </a>
      <?php endif; ?>

    </div>
  </nav>


<!-- Begining of the loop -->
<?php if (have_posts()) : while (have_posts()) : the_post(); ?>

<main id="raw-content">
  <?php the_content(); ?>
</main>

<!-- Contact Form -->
<div class="">

  <form class=" v-padding-small" name="contactForm" id="contactForm" action="#" method="POST">

    <input type="hidden" name="nonceformContact" value="">
    <div class="name">
      <label for="nameContact"><?php esc_html_e('Nom', 'ihag')?> <span>*</span></label>
      <input type="text" name="nameContact" id="nameContact" placeholder="<?php esc_html_e('Martin', 'ihag')?>" required value="">
    </div>
    <div class="firstName">
      <label for="firstNameContact"><?php esc_html_e('Prénom', 'ihag')?> <span>*</span></label>
      <input type="text" name="firstNameContact" id="firstNameContact" placeholder="<?php esc_html_e('Jean', 'ihag')?>" required value="">
    </div>
    <div class="entreprise">
      <label for="nameEntreprise"><?php esc_html_e('Entreprise', 'ihag')?></label>
      <input type="text" name="nameEntreprise" id="nameEntreprise" placeholder="<?php esc_html_e('Nom de la société', 'ihag')?>" value="">
    </div>
    <div class="email">
      <label for="emailContact"><?php esc_html_e('Adresse mail', 'ihag')?> <span>*</span></label>
      <input type="email" name="emailContact" id="emailContact" placeholder="<?php esc_html_e('nom.prenom@exemple.com', 'ihag')?>" required value="">
    </div>
    <div class="tel">
      <label for="tel"><?php esc_html_e('Téléphone', 'ihag')?></label>
      <input type="tel" name="tel" id="tel" placeholder="06 12 34 56 78" value="">
    </div>  
    <div class="message">
      <label for="messageContact"><?php esc_html_e('Message', 'ihag')?> <span>*</span></label>  
      <textarea type="text" name="messageContact" id="messageContact" placeholder="<?php esc_html_e('Rédigez votre message', 'ihag')?>" required rows="8" value=""></textarea>
      <i class="body-like gray-medium"><span>*</span> <?php esc_html_e('Champs obligatoires', 'ihag')?></i>
    </div>  
    
    
    <div class="checkbox">
      <input id="rgpdCheckbox" type="checkbox" required name="rgpdCheckbox">
      <label for="rgpdCheckbox" class="checkbox__label">
        <?php esc_html_e('En cochant cette case et en soumettant ce formulaire, j\'accepte que mes données personnelles soient utilisées pour me recontacter dans le cadre de ma demande indiquée dans ce formulaire. (Aucun autre traitement ne sera effectué avec vos informations)', 'ihag')?>
        <a class="link-default" href="<?php echo get_privacy_policy_url();?>">
          <?php esc_html_e('Voir la politique de confidentialité', 'ihag')?>
        </a>
      </label>
    </div>
    
    <div id="ResponseAnchor" class="center">
      <input class="button" type="submit" id="sendMessage" value="Envoyer le message">
      <p id="ResponseMessage">
        <?php esc_html_e( 'Votre message a été envoyé !', 'ihag' ); ?>
      </p>
    </div>

  </form>

</div>



<!-- End of the loop -->
<?php endwhile; endif;?>

<?php get_footer(); ?>
