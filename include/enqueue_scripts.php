<?php
 
 
add_action('wp_enqueue_scripts', 'site_scripts', 999);

function site_scripts()
{
    global $wp_styles;
    wp_enqueue_style('style', get_template_directory_uri() . "/style.css");

    //Si $rand == 0, alors aucun autre css n'est chargé
    $rand = rand(0, 2);
    if ($rand==1) :
        wp_enqueue_style('style-colored', get_template_directory_uri() . "/style-blue.css"); // Charge le css bleu
    elseif ($rand==2):
        wp_enqueue_style('style-colored', get_template_directory_uri() . "/style-red.css"); // Charge le css rouge
    endif;

    wp_deregister_script('jquery');

    wp_dequeue_style( 'wp-block-library' );

    wp_register_script('script', get_stylesheet_directory_uri() . '/script.js', false, false, 'all');
    wp_enqueue_script('script');
    wp_localize_script('script', 'resturl', get_bloginfo('url') . '/wp-json/ihag/');

}