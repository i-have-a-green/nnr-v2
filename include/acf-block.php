<?php
function my_plugin_block_categories( $categories, $post ) {
    return array_merge(
        $categories,
        array(
            array(
                'slug' => 'ihag',
                'title' => __( 'ihag', 'ihag' ),
                'icon'  => 'star-empty'
            ),
        )
    );
}
add_filter( 'block_categories', 'my_plugin_block_categories', 10, 2 );


add_action('acf/init', 'acf_init_blocs');
function acf_init_blocs() {
    if( function_exists('acf_register_block_type') ) {

        acf_register_block_type(
            array(
                'name'				    => 'titre-texte',
                'title'				    => __('Titre + texte'),
                'description'		    => __('Bloc Gutenberg pour mettre en place un titre h1, h2, h3 ou h4, et un texte en dessous'),
                'placeholder'		    => __('Titre et texte'),
                'render_template'	    => 'template-parts/block/titre-texte.php',
                'category'			    => 'nnr',
                'mode'                  => 'edit',
                'icon'				    => 'menu',
                'keywords'			    => array('titre', 'text'),
                'supports'	            => array(
                                            'align'		=> false,
                                        ),
            )
        );


        acf_register_block_type(
            array(
                'name'				    => 'paragraphe',
                'title'				    => __('Paragraphe'),
                'description'		    => __('Bloc Gutenberg qui permet de créer du texte avec un éditeur de contenus'),
                'placeholder'		    => __('Paragraphe'),
                'render_template'	    => 'template-parts/block/paragraphe.php',
                'category'			    => 'nnr',
                'mode'                  => 'edit',
                'icon'				    => 'media-text',
                'keywords'			    => array('paragraphe', 'text'),
                'supports'	            => array(
                                            'align'		=> false,
                                        ),
            )
        );


        acf_register_block_type(
            array(
                'name'				    => 'video',
                'title'				    => __('Vidéo'),
                'description'		    => __('Permet d’afficher une vidéo à partir d’un lien Youtube. Dans un souci d’éco-conception, on affiche seulement une vignette au chargement de la page. Lorsque l’on clique sur la vignette, le player Youtube apparaît et la vidéo se lance'),
                'placeholder'		    => __('video'),
                'render_template'	    => 'template-parts/block/video.php',
                'category'			    => 'nnr',
                'mode'                  => 'edit',
                'icon'				    => 'format-video',
                'keywords'			    => array('agence', 'bloc', 'video'),
                'supports'	            => array(
                                            'align'		=> false,
                                        ),
            )
        ); 

        acf_register_block_type(
            array(
                'name'				    => 'next-meetup',
                'title'				    => __('Prochain MeetUp - accueil'),
                'description'		    => __('Bloc Gutenberg qui affiche les informations concernant le prochain MeetUp : un illustration, un titre, une date, des informations sur le(s) lieu(x) et un lieu vers l\'inscription.'),
                'placeholder'		    => __('Prochain MeetUp'),
                'render_template'	    => 'template-parts/block/next-meetup.php',
                'category'			    => 'nnr',
                'mode'                  => 'edit',
                'icon'				    => 'bell',
                'keywords'			    => array('titre', 'illustration', 'meetup', 'date', 'lieux', 'lien'),
                'supports'	            => array(
                                            'align'		=> false,
                                        ),
            )
        );

        acf_register_block_type(
            array(
                'name'				    => 'discover',
                'title'				    => __('Découvrir - accueil'),
                'description'		    => __('Bloc Gutenberg composé d\'un titre et d\un texte principales, qui renvoi vers les pages de liste des ressources et des adhérents'),
                'placeholder'		    => __('Découvrir'),
                'render_template'	    => 'template-parts/block/discover.php',
                'category'			    => 'nnr',
                'mode'                  => 'edit',
                'icon'				    => 'migrate',
                'keywords'			    => array('titre', 'texte', 'lien'),
                'supports'	            => array(
                                            'align'		=> false,
                                        ),
            )
        );   
        
        acf_register_block_type(
            array(
                'name'				    => 'news',
                'title'				    => __('Actualités - accueil'),
                'description'		    => __('Bloc Gutenberg pour afficher les 4 dernières actualités sur la page d\'accueil, et un bouton lien vers la page de listing des actualités'),
                'placeholder'		    => __('Acutalités'),
                'render_template'	    => 'template-parts/block/news.php',
                'category'			    => 'nnr',
                'mode'                  => 'edit',
                'icon'				    => 'welcome-widgets-menus',
                'keywords'			    => array('titre', 'actualités', 'lien'),
                'supports'	            => array(
                                            'align'		=> false,
                                        ),
            )
        );

        acf_register_block_type(
            array(
                'name'				    => 'team',
                'title'				    => __('Équipe'),
                'description'		    => __('Bloc Gutenberg pour afficher les membres de \'équipe en les selectionnant parmis les comptes utilisateurs'),
                'placeholder'		    => __('Équipe'),
                'render_template'	    => 'template-parts/block/team.php',
                'category'			    => 'nnr',
                'mode'                  => 'edit',
                'icon'				    => 'groups',
                'keywords'			    => array('team', 'équipe', 'nom', 'description', 'contact'),
                'supports'	            => array(
                                            'align'		=> false,
                                        ),
            )
        );

        acf_register_block_type(
            array(
                'name'				    => 'partners',
                'title'				    => __('Partenaires'),
                'description'		    => __('Liste des partenaires avec une image et un lien'),
                'placeholder'		    => __('Partenaires'),
                'render_template'	    => 'template-parts/block/partners.php',
                'category'			    => 'nnr',
                'mode'                  => 'edit',
                'icon'				    => 'buddicons-buddypress-logo',
                'keywords'			    => array('partenaires', 'titre', 'lien', 'image'),
                'supports'	            => array(
                                            'align'		=> false,
                                        ),
            )
        );

    }
}