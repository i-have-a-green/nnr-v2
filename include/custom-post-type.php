<?php
/* joints Custom Post Type Example
This page walks you through creating
a custom post type and taxonomies. You
can edit this one or copy the following code
to create another one.

I put this in a separate file so as to
keep it organized. I find it easier to edit
and change things if they are concentrated
in their own file.

*/


// ----------------------------------------------------------------------------Custom Post Types start----------------------------------------------------------------------------
// from https://capitainewp.io/formations/developper-theme-wordpress/creer-cpt-theme/ & https://developer.wordpress.org/reference/functions/register_post_type/

/* Custom Post Type Start */
function add_cpt() {

  // Add CPT named 'actuality': register_post_type('slug', array)
  register_post_type( 'resource',  
    array(
      // labels = sentences used in cpt admin
      $labels = array(
        'name' => 'Ressource',
        'all_items' => 'Toutes les ressources',  
        'singular_name' => 'Ressource',
        'add_new_item' => 'Ajouter une ressource',
        'edit_item' => 'Modifier une ressource',
        'menu_name' => 'Ressources',
        // Others
        // 'name_admin_bar' => '',
        // 'add_new' => '',
        // 'add_new_item' => '',
        // 'new_item' => '',
        // 'view_item' => '',
        // 'all_items' => '',
        // 'search_items' => '',
        // 'not_found' => '',
      ),
    
      // list all supports : title, editor, author, thumbnail, excerpt, comments, revisions, custom-fields, page-attributes, post-formats
      $supports = array( 'title', 'editor','thumbnail' ),
      
      'labels' => $labels,
      'supports' => $supports,
      // 'description'=> 'type the CPT description here',
      'public' => true,
      'show_in_rest' => true,
      // 'has archive' => true if it's like an article with 'archive' & 'single'
      'has_archive' => true,
      
      'menu_position' => 7, 
      'menu_icon' => 'dashicons-media-archive',
    )
  );

  // add taxonomy on ressource post type
  $args = array(
    'label'        => __( 'Categories', 'textdomain' ),
    'public'       => true,
    'rewrite'      => false,
    'hierarchical' => true,
  );
  
  register_taxonomy( 'category_resource', 'resource', $args );

}
// Hooking up our function to theme setup
add_action( 'init', 'add_cpt' );
  

// ----------------------------------------------------------------------------Custom Post Types end----------------------------------------------------------------------------

