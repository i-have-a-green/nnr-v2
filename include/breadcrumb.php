<?php
/*
 *  BREADCRUMB // cf. fonctions.php
 *  Basé sur l'extension Breadcrumb NavXT
 */

/*
Affiche le fil d'ariane
*/
//  to include in functions.php
function the_breadcrumb()
{
    $showOnHome = 0; // 1 - show breadcrumbs on the homepage, 0 - don't show
    $delimiter = '<span aria-hidden="true">→<span>'; // delimiter between crumbs
    $home = 'Accueil'; // text for the 'Home' link
    $showCurrent = 1; // 1 - show current post/page title in breadcrumbs, 0 - don't show
    $before = '<span class="current" aria-current="location" itemscope itemtype="http://schema.org/Thing" itemprop="item" itemid="/category/this-page"><span itemprop="name">'; // tag before the current crumb
    $after = '</span></span>'; // tag after the current crumb
    //$pos = 2;

    global $post;
    $homeLink = get_bloginfo('url');
    if (is_home() || is_front_page()) {
        if ($showOnHome == 1) {
            echo '<nav id="nav-breadcrumbs" aria-label="breadcrumbs" class="alignwide"><ol id="breadcrumbs" itemscope itemtype="http://schema.org/BreadcrumbList"><li itemprop="itemListElement" itemscope itemtype="http://schema.org/ListItem"><a href="' . $homeLink . '">' . $home . '</a><meta itemprop="position" content="1" /></li></ol></nav>';
        }
    } else {
        echo '<nav id="nav-breadcrumbs" aria-label="breadcrumbs" class="alignwide"><ol id="breadcrumbs" itemscope itemtype="http://schema.org/BreadcrumbList"><li itemprop="itemListElement" itemscope itemtype="http://schema.org/ListItem"><a itemscope itemtype="http://schema.org/Thing" itemprop="item" href="' . $homeLink . '">' . $home . '</a><meta itemprop="position" content="1" /> ' . $delimiter . ' ';
        if (is_category()) {
            $thisCat = get_category(get_query_var('cat'), false);
            if ($thisCat->parent != 0) {
                echo get_category_parents($thisCat->parent, true, ' ' . $delimiter . ' ');
            }
            echo $before . 'Catégorie "' . single_cat_title('', false) . '"' . $after;
        } elseif (taxonomy_exists(get_query_var('taxonomy') )) {
            $term = get_queried_object();
            $parent = get_post_type_object(get_post_type());
            echo '<a itemscope itemtype="http://schema.org/Thing" itemprop="item" href="' . $homeLink . '/' . $parent->rewrite['slug'] . '/">' . $parent->labels->name . '</a><meta itemprop="position" content="2" /> ' . $delimiter . ' ';
            echo $before . $term->name . $after;
            //  echo "<pre>";
            //  var_dump($term);
            //  echo "</pre>";

        } elseif (is_search()) {
            echo $before . 'Résultat de recherche pour "' . get_search_query() . '"' . $after;
        } elseif (is_day()) {
            echo '<a itemscope itemtype="http://schema.org/Thing" itemprop="item" href="' . get_year_link(get_the_time('Y')) . '">' . get_the_time('Y') . '</a><meta itemprop="position" content="2" /> ' . $delimiter . ' ';
            echo '<a itemscope itemtype="http://schema.org/Thing" itemprop="item"  href="' . get_month_link(get_the_time('Y'), get_the_time('m')) . '">' . get_the_time('F') . '</a><meta itemprop="position" content="2" /> ' . $delimiter . ' ';
            echo $before . get_the_time('d') . $after;
        } elseif (is_month()) {
            echo '<a itemscope itemtype="http://schema.org/Thing" itemprop="item"  href="' . get_year_link(get_the_time('Y')) . '">' . get_the_time('Y') . '</a><meta itemprop="position" content="2" /> ' . $delimiter . ' ';
            echo $before . get_the_time('F') . $after;
        } elseif (is_year()) {
            echo $before . get_the_time('Y') . $after;
        } elseif (is_single() && !is_attachment()) {
            if (get_post_type() != 'post') {
                $post_type = get_post_type_object(get_post_type());
                $slug = $post_type->rewrite;
                echo '<a itemscope itemtype="http://schema.org/Thing" itemprop="item" href="' . $homeLink . '/' . $slug['slug'] . '/">' . $post_type->labels->name . '</a><meta itemprop="position" content="2" />';
                if ($showCurrent == 1) {
                    echo ' ' . $delimiter . ' ' . $before . get_the_title().'<meta itemprop="position" content="3" />'. $after;
                }
            } else {
                $cat = get_the_category();
                $cat = $cat[0];
                $cats = get_category_parents($cat, true, ' ' . $delimiter . ' ');
                if ($showCurrent == 0) {
                    $cats = preg_replace("#^(.+)\s$delimiter\s$#", "$1", $cats);
                }
                echo $cats;
                if ($showCurrent == 1) {
                    echo $before . get_the_title() . $after;
                }
            }
        } elseif (!is_single() && !is_page() && get_post_type() != 'post' && !is_404()) {
            $post_type = get_post_type_object(get_post_type());
            echo $before . $post_type->labels->name . $after;
        } elseif (is_attachment()) {
            $parent = get_post($post->post_parent);
            $cat = get_the_category($parent->ID);
            $cat = $cat[0];
            echo get_category_parents($cat, true, ' ' . $delimiter . ' ');
            echo '<a itemscope itemtype="http://schema.org/Thing" itemprop="item" href="' . get_permalink($parent) . '">' . $parent->post_title . '</a><meta itemprop="position" content="2" />';
            if ($showCurrent == 1) {
                echo ' ' . $delimiter . ' ' . $before . get_the_title() . $after;
            }
        } elseif (is_page() && !$post->post_parent) {
            if ($showCurrent == 1) {
                echo $before . get_the_title() . $after;
            }
        } elseif (is_page() && $post->post_parent) {
            $parent_id  = $post->post_parent;
            $breadcrumbs = array();
            while ($parent_id) {
                $page = get_page($parent_id);
                $breadcrumbs[] = '<a itemscope itemtype="http://schema.org/Thing" itemprop="item" href="' . get_permalink($page->ID) . '">' . get_the_title($page->ID) . '</a><meta itemprop="position" content="2" />';
                $parent_id  = $page->post_parent;
            }
            $breadcrumbs = array_reverse($breadcrumbs);
            for ($i = 0; $i < count($breadcrumbs); $i++) {
                echo $breadcrumbs[$i];
                if ($i != count($breadcrumbs)-1) {
                    echo ' ' . $delimiter . ' ';
                }
            }
            if ($showCurrent == 1) {
                echo ' ' . $delimiter . ' ' . $before . get_the_title() . $after;
            }
        } elseif (is_tag()) {
            echo $before . 'Posts tagged "' . single_tag_title('', false) . '"' . $after;
        } elseif (is_author()) {
            global $author;
            $userdata = get_userdata($author);
            echo $before . 'Articles posté par ' . $userdata->display_name . $after;
        } elseif (is_404()) {
            echo $before . 'Error 404' . $after;
        }
        if (get_query_var('paged')) {
            if (is_category() || is_day() || is_month() || is_year() || is_search() || is_tag() || is_author()) {
                echo ' (';
            }
            echo __('Page') . ' ' . get_query_var('paged');
            if (is_category() || is_day() || is_month() || is_year() || is_search() || is_tag() || is_author()) {
                echo ')';
            }
        }
        echo '</li></ol></nav>';
    }
} // end the_breadcrumb()