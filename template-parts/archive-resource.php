<?php
/**
 * Template part for displaying page archive-post in page.php
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/
 *
 */

?>

<article class="article-resource">

		<?php if(!empty(get_field('link_video'))){ ?>
			<img aria-hidden="true" src="<?php echo get_template_directory_uri(); ?>/image/video.svg" height="32" width="32">
		<?php } else if(!empty(get_field('document'))){?>
			<img aria-hidden="true" src="<?php echo get_template_directory_uri(); ?>/image/document.svg" height="32" width="32">
		<?php } else {?>
			<img aria-hidden="true" src="<?php echo get_template_directory_uri(); ?>/image/link.svg" height="32" width="32">
		<?php } ?>

		<!-- horizontal -->
		<svg preserveAspectRatio="none" class="horizontal" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 1000 5"><g id="Layer_2" data-name="Layer 2"><g id="Layer_1-2" data-name="Layer 1"><rect width="1000" height="5"/></g></g></svg>
		<!-- vertical -->
		<svg preserveAspectRatio="none" class="vertical" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 5 1000"><g id="Layer_2" data-name="Layer 2"><g id="Layer_1-2" data-name="Layer 1"><rect width="5" height="1000"/></g></g></svg>

	<!-- Title -->
	<a class="h6-like link-default ressource-title" href="<?php the_permalink();?>">
		<?php the_title();?>
	</a>

	<!-- dipslay resource type  -->
	<div class="ressource__type">
		<?php 
			$id_post= get_the_ID();
			$cat = ihag_get_term($post, 'category_resource');
			echo $cat->name;
		?>
	</div>
	

	<!-- Date & author -->
	<time class="body-like gray-medium">
		<?php 
		$time_string = 'Publié le <time class="entry-date published updated" datetime="%1$s">%2$s</time>';
		echo sprintf( $time_string,
			esc_attr( get_the_date( DATE_W3C ) ),
			esc_html( get_the_date() ),
			esc_attr( get_the_modified_date( DATE_W3C ) ),
			esc_html( get_the_modified_date() )
		);
		?>
		<span>
			<?php
			echo __(', par ','nnr').get_the_author();
			?>
		</span>
	</time>

	<!-- Display text -->
	<?php //$text = get_field('text'); 
	
	//if ($text):?>
		<!-- <div class="">
			<?php //the_field('text'); ?>
		</div> -->
	<?php //endif; ?>

	<!-- display different button -->
	<div class="button_resources">
	
		<a href="<?php echo the_permalink(); ?>">Voir la ressource ></a>
		
	</div>

	<!-- Title 
	<p class="h6-like link-default">
		<?php the_title();?>
	</p>
	-->
	<!-- Date 
	<time class="body-like gray-medium">
		<?php echo __('Par ','nnr').get_the_author();?>
	</time>
	-->
</article>


