<?php
/**
 * Template part for displaying page archive-post in page.php
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/
 *
 */

?>

<article class="article-post">

	<!-- Thumbnail -->
	<a class="thumbnail" href="<?php the_permalink();?>" title="<?php the_title();?>">
		<?php 
			if ( has_post_thumbnail() ) { ?>
				<img src="<?php echo get_the_post_thumbnail_url($post, 'medium');?>" alt="<?php echo get_the_post_thumbnail_caption();?>">
			<?php
			} else {
				$image = get_field('imageFallback', 'option');
				$size = 'medium';
				echo wp_get_attachment_image( $image, $size );
			} ?>
			<a class="button" href="<?php the_permalink();?>"><?php _e('Lire l\'article >', 'nnr') ?></a>
	</a>

	<!-- Title -->
	<a class="h6-like link-default" href="<?php the_permalink();?>">
		<?php the_title();?>
	</a>
	
	<!-- Date -->
	<time class="body-like gray-medium" datetime="<?php echo get_the_date('c'); ?>">
		<?php echo __('Publié le ','nnr').get_the_date();?>
	</time>

</article>


