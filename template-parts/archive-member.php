<?php
/**
 * Template part for displaying page archive-member in page.php
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/
 *
 */

?>
 
<?php 
	$user_image = get_field('image', $user);
	$image_by_default = get_field('imageFallback', 'options');
	$user_name = $user->display_name;
	$user_sector = get_field('sector', $user);
?>

<!-- user card -->
<div class="userCard">

	<a href="<?php echo get_author_posts_url($user->ID); ?>">
		<!-- image -->
		<div class="iconContainer">
			<?php if (!empty ($user_image)) : ?>
				<?php echo wp_get_attachment_image( $user_image, 'icon-member' ); 
				else :
					echo wp_get_attachment_image( $image_by_default, 'icon-member' );
				endif; 
				?>                       
		</div>
		<!-- name -->
		<p><?php echo $user_name; ?></p>

		<!-- sector -->
		<?php if (!empty ($user_sector)) : ?>
			<p><?php echo $user_sector['label']; ?></p>
		<?php endif; ?>
	</a>


</div>


