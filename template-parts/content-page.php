<?php
/**
 * Template part for displaying page content in page.php
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/
 *
 */

?>
<!-- NOT USED 
	- MAY DELETE LATER -

<article id="post-<?php the_ID(); ?>" <?php post_class('wrapper'); ?>>

	<?php if ( !is_front_page() ){
		echo '<header class="wrapper v-padding-regular">';?>
		<!-- Fil d'Ariane -->
		<?php if (function_exists('the_breadcrumb')) the_breadcrumb(); ?>
		<?php
		the_title('<h1 class="center"', '</h1>');
		echo '</header>';
	} ?>

	<?php the_content(); ?>

</article>
	
-->
