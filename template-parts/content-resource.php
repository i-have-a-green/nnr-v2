<?php
/**
 * Template part for displaying page content-resource in page.php
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/
 *
 */

?>

<article class="article-resource">

	<!-- Fil d'Ariane -->
	<?php if (function_exists('the_breadcrumb')) the_breadcrumb(); ?>

	<!-- Title -->
	<h2 class="link-default ressource-title" >
		<?php the_title();?>
	</h2>

	<!-- dipslay resource type  -->
	<?php 
		$id_post= get_the_ID();
		$cat = ihag_get_term($post, 'category_resource');
		echo $cat->name;
	?>

	<fieldset>
		<!-- Date & author -->
		<time class="body-like gray-medium">
			<p>
				<?php 
				$time_string = 'Publié le: <time class="entry-date published updated" datetime="%1$s">%2$s</time>';
				echo sprintf( $time_string,
					esc_attr( get_the_date( DATE_W3C ) ),
					esc_html( get_the_date() ),
					esc_attr( get_the_modified_date( DATE_W3C ) ),
					esc_html( get_the_modified_date() )
				);
				?>
			</p>
			<span>
				<?php
				echo __('Auteur: ','nnr').get_the_author();
				?>
			</span>
		</time>
	</fieldset>

	<!-- If image : display image -->
	<?php
	$image = get_field('image');

	if ( $image ):?>
		<div class="wrapper">
			<?php 
			$size = 'large';
			echo wp_get_attachment_image($image, $size); 
			?> 
		</div>
	<?php endif; ?>

	<!-- If text : display text -->
	<?php $text = get_field('text'); 
	
	if ($text):?>
		<div class="">
			<?php the_field('text'); ?>
		</div>
	<?php endif; ?>

	<!-- display different buttons -->
	<div class="button_resources">
		<?php if(!empty(get_field('link_video'))){?>
		<!-- btn video -->

			<?php $block_uniq_id = "id_".uniqid(); ?>

			<a class="btn-modale no-player button_video button" href="#" data-uniq-id="<?php echo $block_uniq_id;?>">
				<?php _e('Voir la vidéo >', 'nnr'); ?>
				
				<script> 
					if (typeof iframe === 'undefined') {
					var iframe = new Object();
					}
					iframe.<?php echo $block_uniq_id;?> = '<?php echo get_field('link_video'); ?>'; 
				</script>
	
			</a>
		
		<?php } else if(!empty(get_field('document'))){?>
			<!-- btn document  -->

			<a class="button_document button" href="<?php the_field('document') ?>" download><?php _e('Télécharger le document >', 'nnr'); ?></a>
		
		<?php } else {?>
			<!-- btn link  -->
		
			<?php $link = get_field('link');
			$link_url = $link['url'];
			$link_target = $link['target'] ? $link['target'] : '_self';
			?>

			<a class="button_link button" href="<?php echo esc_url( $link_url ); ?>" target="<?php echo esc_attr( $link_target ); ?>"><?php _e('Ouvrir le lien >', 'nnr');  ?></a>

		<?php } ?>
	</div>

</article>


