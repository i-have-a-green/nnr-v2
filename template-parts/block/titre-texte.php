<?php
/**
 * Block Name: Titre_texte
 */
 ?>

<?php
$title = get_field('title');
$html = get_field('level_title');
$text = get_field('text');

if ( empty($title) ):?>
	<em>Renseigner le titre</em>


<?php else :
	// title
	echo "<". $html ." class='custom-title'>". $title ."</". $html .">";?>
<div class="title-text">
	<!-- text -->
	<?php echo $text;?>
</div>	

<?php endif; ?>

