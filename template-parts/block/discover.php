<?php
/**
 * Block Name: discover
 */
 ?>

<?php
$title = get_field('title');
$text = get_field('text');

if ( empty($title) ): ;?>
	<em>Renseigner le titre</em>
<?php else : ;?>
<div class="discover__block">
	<!-- title -->
	<div class="title__content">
		<h2><?php echo $title ;?></h2>
		<svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 621.12 72.73"><defs></defs><g data-name="Layer 2"><g data-name="Layer 1"><polygon class="cls-1" points="466.72 72.73 358.72 18.73 313.72 72.73 205.72 18.73 160.72 72.73 52.72 18.73 11.52 68.17 0 58.56 48.8 0 156.8 54 201.8 0 309.8 54 354.8 0 462.8 54 507.8 0 621.12 56.66 614.41 70.07 511.72 18.73 466.72 72.73"/></g></g></svg>
	</div>

	<!-- text -->
	<!-- <p><?php echo $text ;?></p> -->

	<!-- Repeter discover other pages -->
	<?php if( have_rows('discover') ): ;?>

		<div class="content">

			<?php while( have_rows('discover') ) : the_row();?>

				<?php $icon = get_sub_field('icon');
				$name = get_sub_field('name');
				$link = get_sub_field('link');?>

				<div class="solo">

					<!-- Image -->
					<div class="icon">
						<?php echo wp_get_attachment_image($icon, ''); ?>
					</div>

					<!-- Name -->
					<p><?php echo $name ;?></p>

					<!-- link -->
					<?php
					if( $link ): 
						$link_url = $link['url'];
						$link_title = $link['title'];
						$link_target = $link['target'] ? $link['target'] : '_self';
						?>
						<a class="button" href="<?php echo esc_url( $link_url ); ?>" target="<?php echo esc_attr( $link_target ); ?>"><?php echo esc_html( $link_title ); ?></a>
					<?php endif; ?>
				
				</div>

			<?php endwhile ;?>
		
		</div>

	<?php endif ;?>
</div>
<?php endif; ?>


