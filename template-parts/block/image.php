<?php
/**
 * Block Name: Bloc Image
 */
 ?>

<section class="blk-img v-padding-small">

<?php
$image = get_field('image');

if ( !$image ):?>
	<em>Renseigner l'image</em>
	
<?php else :?>

	<div class="wrapper">
	
		<?php 
		$size = 'large';
		echo wp_get_attachment_image($image, $size); ?> 
	
	</div>

<?php endif; ?>
</section>
