<?php
/**
 * Block Name: Bloc Video
 * 
 * https://gist.github.com/soderlind/573439391c4d18b5ffbd
 * 
 */
 ?>

<?php $block_uniq_id = "id_".uniqid(); ?>


<section id="block_<?php echo $block_uniq_id; ?>_section" class="blk-video">

        <?php
        $title = get_field('title');
        $text = get_field('text');
        $link = get_field('link');
        $video = get_field('link_video');
        if ( !$video ) :?>
            <em>Renseigner le bloc</em>            
        <?php else :?>

            <div class="video-section">

                <div class="title__content">
                <!-- Title -->
                    <h2><?php echo $title ;?></h2>
                    <svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 621.12 72.73"><defs><style>.cls-1{fill:#f0a6ff;}</style></defs><g data-name="Layer 2"><g data-name="Layer 1"><polygon class="cls-1" points="466.72 72.73 358.72 18.73 313.72 72.73 205.72 18.73 160.72 72.73 52.72 18.73 11.52 68.17 0 58.56 48.8 0 156.8 54 201.8 0 309.8 54 354.8 0 462.8 54 507.8 0 621.12 56.66 614.41 70.07 511.72 18.73 466.72 72.73"/></g></g></svg>
                </div>
                <div class="video-container">

                    <?php 
                    // 01 -Test if thumbnail_video doesn't exists
                    if (!get_field('thumbnail_video')) {    
                    ?>
                        <a href="#" title="Ouverture d'une boîte modale pour le lecteur vidéo" class="btn-modale" data-uniq-id="<?php echo $block_uniq_id;?>">
                            <script> 
                                if (typeof iframe === 'undefined') {
                                var iframe = new Object();
                                }
                                iframe.<?php echo $block_uniq_id;?> = '<?php echo get_field('link_video'); ?>'; 
                            </script>
        
                            <?php
                                $video = get_field( 'link_video' );
                                $id = retrieve_id_video($video);
                            ?>
        
                            <img alt='Vignette de la vidéo de référence' src="https://img.youtube.com/vi/<?php echo $id; ?>/mqdefault.jpg">
        
                        </a> 
                    
                    <?php 
                    } else { 
                    // 02 - If we have a thumbnail
                    ?>
    
                        <a href="#" title="Ouverture d'une boîte modale pour le lecteur vidéo" class="btn-modale" data-uniq-id="<?php echo $block_uniq_id;?>">
                            <script> 
                                if (typeof iframe === 'undefined') {
                                    var iframe = new Object();
                                }
                                iframe.<?php echo $block_uniq_id;?> = '<?php echo get_field('link_video'); ?>'; 
                            </script>

                            <?php
                            $videoThumbnail = get_field('thumbnail_video');
                            $size = 'medium';
                            echo wp_get_attachment_image($videoThumbnail, $size);?>
                        </a>

                    <?php } ?>
    
                <!-- /embed-container -->
                
                <!-- text -->
                <p><?php echo $text;?></p>

                <!-- link -->
                <?php
					if( $link ): 
						$link_url = $link['url'];
						$link_title = $link['title'];
						$link_target = $link['target'] ? $link['target'] : '_self';
						?>
						<a class="button" rel="noreferrer" title="Ouverture d'un nouvel onglet" href="<?php echo esc_url( $link_url ); ?>" target="<?php echo esc_attr( $link_target ); ?>"><?php echo esc_html( $link_title ); ?></a>
					<?php endif; ?>
                </div>
            </div>

        <?php endif; ?>

</section>