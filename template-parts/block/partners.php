<?php
/**
 * Block Name: Partenaires 
 */
 ?>

<?php
$title = get_field('title');
$text = get_field('text');
?>

<div class="blk-partner">

<?php
if ( empty($title) ):?>
    <em>Renseigner le bloc</em>
<?php else :?>

    <?php // Title
    $title = get_field('title');

    if($title) {?>

        <div class="title__content">
			<h2><?php echo $title ;?></h2>
			<svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 621.12 72.73"><defs><style>.cls-1{fill:#f0a6ff;}</style></defs><g data-name="Layer 2"><g data-name="Layer 1"><polygon class="cls-1" points="466.72 72.73 358.72 18.73 313.72 72.73 205.72 18.73 160.72 72.73 52.72 18.73 11.52 68.17 0 58.56 48.8 0 156.8 54 201.8 0 309.8 54 354.8 0 462.8 54 507.8 0 621.12 56.66 614.41 70.07 511.72 18.73 466.72 72.73"/></g></g></svg>
		</div>

    <?php }?>
    
    <?php // Check rows exists.
        if( have_rows('partners') ):?>
        
        <div class="basic-listing">
        
            <?php 
            // Loop through rows.
            while( have_rows('partners') ) : the_row();
                $image = get_sub_field('image');
                $link = get_sub_field('link');
                if( $link ): 
                    $link_url = $link['url'];
                    $link_target = $link['target'] ? $link['target'] : '_self';
                    ?>
                    <div class="solo-partner">
                        <a rel="noreferrer" href="<?php echo esc_url( $link_url ); ?>" target="<?php echo esc_attr( $link_target ); ?>">
                    
                    
                <?php endif;

                echo wp_get_attachment_image($image, 'medium'); 

                if( $link ):?> 
                    </a>
                <?php endif; ?>
                </div>
            <?php
            // End loop.
            endwhile;?>
        
        <div>
        
        <?php
        // No value.
        else :
            // Do something...
        endif;?> 
	
<?php endif; ?>

</div>

