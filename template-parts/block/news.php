<?php
/**
 * Block Name: news
 */
 ?>

<?php
$title = get_field('title');

if ( empty($title) ): ;?>
	<em>Renseigner le titre</em>
<?php else : ;?>

	<div class="posts__list-block">
		<!-- title -->
		<div class="title__content">
			<h2><?php echo $title ;?></h2>
			<svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 621.12 72.73"><defs><style>.cls-1{fill:#f0a6ff;}</style></defs><g data-name="Layer 2"><g data-name="Layer 1"><polygon class="cls-1" points="466.72 72.73 358.72 18.73 313.72 72.73 205.72 18.73 160.72 72.73 52.72 18.73 11.52 68.17 0 58.56 48.8 0 156.8 54 201.8 0 309.8 54 354.8 0 462.8 54 507.8 0 621.12 56.66 614.41 70.07 511.72 18.73 466.72 72.73"/></g></g></svg>
		</div>

		<!-- Load 4 last posts -->
		<?php 
		$args = array(
			'post_type' => 'post',
			'posts_per_page' => 4,
			'order' => 'ASC',
		);

		if($args):?>
			<div class="posts__listing-main"> 
				<?php query_posts($args);
				global $wp_query; 
				if ( have_posts() ) : while (have_posts()) : the_post();?>
					
				<!-- Display posts -->
				<article class="article-post">

					<!-- Thumbnail -->
					<a class="thumbnail" href="<?php the_permalink();?>" title="<?php the_title();?>">
						<?php 
							if ( has_post_thumbnail() ) { ?>
								<img src="<?php echo get_the_post_thumbnail_url($post, 'medium');?>" alt="<?php echo get_the_post_thumbnail_caption();?>">
							<?php
							} else {
								$image = get_field('imageFallback', 'option');
								$size = 'medium';
								echo wp_get_attachment_image( $image, $size );
							} 
						?>
					</a>
								
					<!-- Title -->
					<a class="h6-like link-default" href="<?php the_permalink();?>">
						<?php the_title();?>
					</a>
					
					<!-- Date -->
					<time datetime="<?php echo get_the_date('Y-m-d'); ?>" class="body-like gray-medium">
						<?php echo __('Publié le ','nnr').get_the_date();?>
					</time>
				
				</article>

				<?php endwhile; endif;?>
			</div>
		<?php endif; ?>

	

		<!-- link -->
		<?php $link = get_field('link');?>
		<?php
		if( $link ): 
			$link_url = $link['url'];
			$link_title = $link['title'];
			$link_target = $link['target'] ? $link['target'] : '_self';
			?>
			<a class="button" href="<?php echo esc_url( $link_url ); ?>" target="<?php echo esc_attr( $link_target ); ?>"><?php echo esc_html( $link_title ); ?></a>
		<?php endif; ?>
					
	</div>

<?php endif; ?>


