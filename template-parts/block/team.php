<?php
/**
 * Block Name: Équipe
 */
 ?>

<!-- Titre-->
<div class="title__container">
	<div class="title__content">
		<h2><?php _e('Nos équipiers', 'nnr'); ?></h2>
		<svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 621.12 72.73"><defs><style>.cls-1{fill:#f0a6ff;}</style></defs><g data-name="Layer 2"><g data-name="Layer 1"><polygon class="cls-1" points="466.72 72.73 358.72 18.73 313.72 72.73 205.72 18.73 160.72 72.73 52.72 18.73 11.52 68.17 0 58.56 48.8 0 156.8 54 201.8 0 309.8 54 354.8 0 462.8 54 507.8 0 621.12 56.66 614.41 70.07 511.72 18.73 466.72 72.73"/></g></g></svg>
	</div>
</div>


<?php
// Check rows exists.
if( have_rows('users') ): ?>

	<div class="member__container">
	<?php
    // Loop through rows.
    while( have_rows('users') ) : the_row();

        $user_id = get_sub_field('user');
		$user_meta = get_user_meta($user_id);
		$image = $user_meta['image'];
		$image_by_default = get_field('imageFallback', 'options');
		$first_name = $user_meta['first_name'];
		$last_name = $user_meta['last_name'];
		$description = $user_meta['description'];
		$sector = $user_meta['sector'];
		$site = get_field('site', 'user_'.$user_id);
		$twitter = get_field('twitter', 'user_'.$user_id);
		$linkedin = get_field('linkdin', 'user_'.$user_id);
		$facebook = get_field('facebook', 'user_'.$user_id);
		// var_dump ($user_meta);?>

		<!-- display card -->
		<div class="member__card">

			<!-- image -->
			<div class="member__image">
				<?php if (!empty ($image[0])) : ?>
					<?php echo wp_get_attachment_image( $image[0], 'icon-team' ); 
					else :
						echo wp_get_attachment_image( $image_by_default, 'icon' );
					endif; 
				?>    
			</div>
			<!-- text / desc / links -->
			<div>
				<!-- name -->
				<div class="name">
					<?php if (!empty ($image[0])) : ?>
						<p><?php echo $first_name[0].' '.$last_name[0] ;?></p>
					<?php endif; ?>  
				</div>
				<!-- sector -->
				<!-- <?php if (!empty ($image[0])) : ?>
					<p><b><?php echo $sector[0];?></b></p>
				<?php endif; ?>   -->
				
				
				<!-- desc -->
				<div class="member__desc">
					<?php if (!empty ($image[0])) : ?>
						<p><?php echo $description[0];?></p>
					<?php endif; ?>  
				</div>
					
				<!-- contact -->
				<div class="member__contact">
					<?php if (!empty($site)) :
						$link_url_site = $site['url'];
						$link_title_site = $site['title'];
						$link_target_site = $site['target'] ? $site['target'] : '_self';
						?>
						
						<a class="" href="<?php echo esc_url( $link_url_site ); ?>" target="<?php echo esc_attr( $link_target_site ); ?>">
							<img aria-hidden="true" src="<?php echo get_template_directory_uri(); ?>/image/site.svg" height="24" width="24">
						</a>
					<?php endif; ?>

					<?php if (!empty($twitter)) :
						$link_url_twitter = $twitter['url'];
						$link_title_twitter = $twitter['title'];
						$link_target_twitter = $twitter['target'] ? $twitter['target'] : '_self';
						?>
						
						<a class="" href="<?php echo esc_url( $link_url_twitter ); ?>" target="<?php echo esc_attr( $link_target_twitter ); ?>">
							<img aria-hidden="true" src="<?php echo get_template_directory_uri(); ?>/image/twitter.svg" height="24" width="24">
						</a>
					<?php endif; ?>

					<?php if (!empty($linkedin)) :
						$link_url_linkdin = $linkedin['url'];
						$link_title_linkdin = $linkedin['title'];
						$link_target_linkdin = $linkedin['target'] ? $linkdin['target'] : '_self';
						?>
						
						<a class="" href="<?php echo esc_url( $link_url_linkdin ); ?>" target="<?php echo esc_attr( $link_target_linkdin ); ?>">
							<img aria-hidden="true" src="<?php echo get_template_directory_uri(); ?>/image/linkedin.svg" height="24" width="24">
						</a>
					<?php endif; ?>

					<?php if (!empty($facebook)) :
						$link_url_facebook = $facebook['url'];
						$link_title_facebook = $facebook['title'];
						$link_target_facebook = $facebook['target'] ? $facebook['target'] : '_self';
						?>
						
						<a class="" href="<?php echo esc_url( $link_url_facebook ); ?>" target="<?php echo esc_attr( $link_target_facebook ); ?>">
							<img aria-hidden="true" src="<?php echo get_template_directory_uri(); ?>/image/facebook.svg" height="24" width="24">
						</a>
					<?php endif; ?>
				
				</div>

			</div>


		</div>

    <?php 
	// End loop.
    endwhile;?>

	</div>

	<?php
// No value.
else :
    // Do something...
endif;

