<?php
/**
 * Block Name: next-meetup
 */
 ?>

<?php
$image = get_field('image');
$title = get_field('title');
$date = get_field('date');
$location = get_field('location');
$link = get_field('link');



if ( empty($date) ): ;?>
	<em>Renseigner le titre</em>
<?php else : ;?>
<div class="meetup__block">
	<!-- Title with decoration -->
	<div class="title__content">
		<h2>Prochain Meetup</h2>
		<svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 621.12 72.73"><defs><style>.cls-1{fill:#f0a6ff;}</style></defs><g data-name="Layer 2"><g data-name="Layer 1"><polygon class="cls-1" points="466.72 72.73 358.72 18.73 313.72 72.73 205.72 18.73 160.72 72.73 52.72 18.73 11.52 68.17 0 58.56 48.8 0 156.8 54 201.8 0 309.8 54 354.8 0 462.8 54 507.8 0 621.12 56.66 614.41 70.07 511.72 18.73 466.72 72.73"/></g></g></svg>
	</div>
	<!-- Image part on the left -->
	<div class="meetup-section">
		<div class="meetup-img">
			<?php echo wp_get_attachment_image($image, 'medium'); ?>
		</div>

		<!-- Text part on the right -->
		<div class="meetup-content">
			<!-- title -->
			<h3><?php echo $title;?></h3>

			<!-- date & location -->
			<p><?php echo $date.' - '.$location;?></p>

			<!-- link -->
			<?php
			if( $link ): 
				$link_url = $link['url'];
				$link_title = $link['title'];
				$link_target = $link['target'] ? $link['target'] : '_self';
				?>
				<a class="button" rel="noreferrer" href="<?php echo esc_url( $link_url ); ?>" target="<?php echo esc_attr( $link_target ); ?>"><?php echo esc_html( $link_title ); ?></a>
			<?php endif; ?>
		</div>
	</div>
</div>
<?php endif; ?>