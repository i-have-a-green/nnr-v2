<?php
/**
 * Template part for displaying page content in page.php
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/
 *
 */

?>

<article id="post-<?php the_ID(); ?>"<?php post_class('wrapper'); ?>>
	
	<div>
		<!-- Fil d'Ariane -->
		<?php if (function_exists('the_breadcrumb')) the_breadcrumb(); ?>

		<!-- title -->
		<a href="<?php the_permalink();?>"><h2><?php the_title();?></h2></a>

		<!-- Meta -->
		<div class="entry-meta">
			<?php
			$time_string = 'Publié le <time class="entry-date published updated" datetime="%1$s">%2$s</time>';
			echo sprintf( $time_string,
				esc_attr( get_the_date( DATE_W3C ) ),
				esc_html( get_the_date() ),
				esc_attr( get_the_modified_date( DATE_W3C ) ),
				esc_html( get_the_modified_date() )
			);
			echo ", rédigé par ".esc_html( get_the_author() );
			?>
		</div>

		<!-- Thumbnail -->
		<a class="thumbnail image-container" href="<?php the_permalink();?>" title="<?php the_title();?>">
			<?php 
				$size = 'free-height';
				if ( has_post_thumbnail() ) { ?>
					<img src="<?php echo get_the_post_thumbnail_url($post, $size);?>" alt="<?php echo get_the_post_thumbnail_caption();?>">
				<?php
				} else {
					$size = 'thumbnail-standard';
					$image = get_field('imageFallback', 'option');
					echo wp_get_attachment_image( $image, $size );
				} 
			?>
		</a>

		<?php if(is_single()):?>

			<?php the_content();?>
		
		<?php //else:?>
		
			<?php //the_excerpt();?>
		
		<?php endif;?>
		
	</div>

</article>

